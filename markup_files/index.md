

![BIld 1](media/1.jpeg)

**3D-projekterade anläggningsmodeller för maskinstyrning/guidning effektiviserar anläggningsbranschen. Tryck på bilden ovan för att aktivera anläggningsmodellen.**

#  Anläggningsmodell i 3D underlättar maskinstyrning

> ##### Med hjälp av en 3D-projekterad anläggningsmodell, som upprättats och levererats av en projektör, kan entreprenören direkt använda den för maskinstyrning/guidning. Tidigare fel vid manuellt framtagna modeller undviks samtidigt som anläggningsarbetet blir betydligt billigare för beställaren.

– TIDIGARE HAR MAN INTE HAFT NÅGON väldefinierad struktur i anläggningsmodellen och kvalitetssäkringsmetoder har helt saknats. Nu har vi arbetat fram krav och metoder för hur man framställer en sådan, säger Daniel Nilsson, 3D-projektör och
projektledare för forsknings- och utvecklingsprojektet ”Underlag till entreprenör gällande maskinstyrning/guidning”.
​	Maskinstyrning/guidning, det vill säga att använda en dator i en anläggningsmaskin i kombination med mätutrustning som GPS för att styra maskinen, har använts mer och mer under de senaste tio åren. Maskinstyrningsmodellen har vanligtvis tagits fram av entreprenören utifrån de ritningar som projektören
levererat. Arbetet med att tolka, mäta och konvertera innebär mycket jobb för mätteknisk personal hos entreprenö- ren. Dessutom kan det manuella arbetet lätt leda till fel. 
​	Projektörerna har jobbat i 3D ganska länge men det har
aldrig funnits några krav på att de ska leverera digitalt material som gör maskinstyrning/guidning möjlig. Daniel Nilsson menar att det funnits en utbredd okunskap om hur flödet mellan konsult och entreprenör fungerar. Men under de senaste åren har entreprenörerna börjat efterfråga underlag som är anpassat
till maskinstyrning/guidning.
​	– Vi bör förädla och leverera det arbete vi faktiskt utför och kan prestera. För att teknikerna ska fungera krävs lite mer när man bygger upp en projektering ; man måste tänka på nya viktiga frågor som att använda rätt metod och kodning. Olika delar
i en anläggningsmodell har olika namn som entreprenören kan förstå i en teoretisk modell. Här finns många fördelar, till exempel kan man koppla en schaktmodell till en AMA-kod för att se vilket arbete som ska utföras. Man kan även nyttja den 3Dprojekterade anläggingsmodellen till andra tillämpningar som mängdberäkning, mängdreglering, uppföljning och kontroll. 
​	Entreprenören efterfrågar en leverans med hög kvalité och att man får färdiga triangulerade ytor och linjemodeller med
kodning på varje linje och yta. Både ytmodellen och linjemodellen ska kvalitetssäkras på ett speciellt sätt innan konsulten släpper dem ifrån sig.
​	– Konsulten måste kvalitetssäkra leverans till entreprenör genom att utföra ett antal kontroller som egenkontroll, teknisk granskning och mätningsteknisk kontroll. Den mätningstekniska kontrollen innebär exempelvis att filformat, filstorlek, geotekniskt läge och exportkvalité kontrolleras. 

MÅLET MED FORSKNINGS- OCH UTVECKLINGSPROJEKTET har varit att ta fram krav och metod för hur en 3D-projekterad anläggningsmodell ska framställas, granskas och kontrolleras för att vara kvalitetssäkrad och anpassad för att levereras till en entreprenör som ska använda den för maskinstyrning/guidning.
​	– Krav och metod för upprättande av anläggningsmodell finns i upphandlingsdokumentet ”Krav för upprättande av anläggningsmodell”. Vi har tagit fram en ny struktur, en teknisk beskrivning, för hur anläggningsmodeller ska utformas och hanteras, säger Daniel Nilsson.
​	Denna struktur har testats i ett konkret anläggningsprojekt – femhundra meter av vägsträckan Markaryd-Osby. En 3D-projekterad anläggningsmodell togs fram med olika slags projekteringsverktyg – Civil3D, Inroads samt Novapoint.
​	En anläggningsmodell upprättades utifrån de krav som fastställts i projektet och metoderna verifierades genom att de följdes upp på plats. De olika programvaror och maskinstyrningsdatorer som finns i anläggningsmaskinerna har testats och visat sig fungera väl. 
​	Man måste säkerställa att man har rätt storlek på datamängden, rätt filformat, rätt kodning och inte har dubbelpunkter i modellerna. Man måste även göra ett antal tester för att se att överföringen mellan programvarorna fungerar. 

Trafikverket och andra beställare kan nu ställa nya krav på konsulterna vilket leder till att de måste prestera lite mer och jobba på ett delvis annat sätt jämfört med tidigare. Det innebär även att beställaren kan ta med detta i upphandlingen av entreprenör.
Genom att meddela att det finns en färdig anläggningsmodell kan man som beställare hoppas på att få lägre kostnad för produktionen. Det är mycket värt att kunna spara några procent i produktionen jämfört med en något dyrare konsultinsats 
Det finns dock fortfarande alltför få konsulter som har full koll på 3D-projektering, påpekar Daniel Nilsson.

PROJEKTRESULTATEN HAR GETT EN BRA ANSATS och en bra plattform att arbeta vidare från. Erfarenheter av olika testkörningar kommer förmodligen att leda till än fler forsknings- och utvecklingsprojekt där metoderna kan förädlas. Daniel Nilsson tror
att implementeringen av den framtagna modellen kommer att ske snabbt eftersom det finns ett stort intresse för de här frågorna. Många teknikleverantörer i branschen är engagerade.
​	– Detta är en viktig tillämpning av ”Virtuellt Byggande” och ”AnläggningsInformationsModeller” inom anläggningsbranschen. Det är ofta stora massor som ska schaktas och det ligger mycket pengar för entreprenören i det arbetet. Vår modell leder till att man kan bygga effektivare och att man blir intresserad och vill gå vidare med ytterligare informationskopplingar som hör samman med detta.
Oktober 2010 													Göran Nilsson

EN KONTROLLMÄTNING VISADE EN höjdmedelavvikelse på 3,7 cm för en grovschaktning med GPS-teknik. Ett tillräckligt bra resultat för grovschaktning, menar Daniel Nilsson. När man använder maskinstyrning för överbyggnadslagren ställs dock andra krav. När entreprenören bygger upp dessa lager går noggrannhetskraven från centimeter- till millimeternivå. 
​	– Den metod vi utvecklat utgör ett ramverk och skapar förutsättningar för att fler ska lyckas med styrning och guidning. Det behövs tydliga riktlinjer för hur projektörer ska jobba och vad man ska leverera. Nu har vi erfarenhet av att det faktiskt går att lösa det här med olika projekteringsverktyg och att det finns grundkriterier för hur man ska jobba som konsult för att kunna göra en leverans som fungerar fullt ut, vilket är ett stort steg på vägen inom det här området. Det upphandlingsdokument som vi utformat används redan nu i flera stora infrastrukturprojekt.